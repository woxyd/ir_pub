package cz.zcu.kiv.nlp.ir;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import org.apache.log4j.Logger;

public class DomXmlOutput {

	private Document doc;
	private Element root, recenze, opravy, shrnuti, text;
	private String xmlString;

	private static final Logger log = Logger.getLogger(Crawler.class);
	
	public DomXmlOutput() {
		try {
			/////////////////////////////
			// Creating an empty XML Document

			// We need a Document
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			doc = docBuilder.newDocument();

			////////////////////////
			// Creating the XML tree

			// create the root element and add it to the document
			/*
			 * root = doc.createElement("root"); doc.appendChild(root);
			 */

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public Element getRoot() {
		return root;
	}

	public Document getDoc() {
		return doc;
	}

	public void createRootElement(String name) {
		root = doc.createElement(name);
		doc.appendChild(root);
	}

	public void createRecenzeElement(String name) {
		recenze = doc.createElement(name);
		root.appendChild(recenze);
	}
	
	public void createOpravyElement(String name, String textValue) {
		opravy = doc.createElement(name);
		recenze.appendChild(opravy);
		Text text = doc.createTextNode(textValue);
		opravy.appendChild(text);
	}
	
	public void createShrnutiElement(String name, String textValue) {
		shrnuti = doc.createElement(name);
		recenze.appendChild(shrnuti);
		Text text = doc.createTextNode(textValue);
		shrnuti.appendChild(text);
	}
	
	public void createBodyElement(String name, String textValue) {
		text = doc.createElement(name);
		recenze.appendChild(text);
		Text inText = doc.createTextNode(textValue);
		text.appendChild(inText);
	}

	public void createTextElement(String name) {
		if (root == null) {
			System.out.println("Neni jeste root element");
			System.exit(1);
		}
		Text text = doc.createTextNode(name);		
		Node node = root.getFirstChild();
		node.appendChild(text);
	}

	public void createNestedElementWithText(String name, String text, int level) {
		switch (level) {
		case 1:

		}
	}

	/*
	 * public void createComment(Element elm) { // create a comment and put it
	 * in the root element Comment comment = doc.createComment("Just a thought"
	 * ); elm.appendChild(comment); }
	 * 
	 * public void createChild(Element ancestor, String elName, String attrName,
	 * String attrValue, String text) { // create child element, add an
	 * attribute, text and add to root Element elm = doc.createElement(elName);
	 * elm.setAttribute(attrName, attrValue); ancestor.appendChild(elm);
	 * 
	 * // add a text element to the child Text textNode =
	 * doc.createTextNode(text); elm.appendChild(textNode); }
	 * 
	 * public void createChild(Element ancestor, String elName, String text) {
	 * // create child element, add an text, and add to root Element elm =
	 * doc.createElement(elName); ancestor.appendChild(elm);
	 * 
	 * // add a text element to the child Text textNode =
	 * doc.createTextNode(text); elm.appendChild(textNode); }
	 * 
	 * public void createChild(Element ancestor, String elName, String text,
	 * String newRoot) { // create child element, add an text, and add to root
	 * Element elm = doc.createElement(elName); ancestor.appendChild(elm);
	 * 
	 * // add a text element to the child Text textNode =
	 * doc.createTextNode(text); elm.appendChild(textNode); root = elm; }
	 */

	public String createXML() {
		/////////////////
		// Output the XML

		// set up a transformer
		try {

			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans;
			trans = transfac.newTransformer();

			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");
			trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			// create string from xml tree
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
			xmlString = sw.toString();

			// print xml
			//log.info("Nahled xml souboru: " + xmlString);
			
			
			

		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return xmlString;
	}
}
