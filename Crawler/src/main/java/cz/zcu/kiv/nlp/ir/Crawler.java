package cz.zcu.kiv.nlp.ir;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


import java.io.*;
import java.util.*;

/**
 * Crawler class acts as a controller. You should only adapt this file to serve your needs.
 * Created by Tigi on 31.10.2014.
 */
public class Crawler {
    /**
     * Xpath expressions to extract and their descriptions.
     */
    private final static Map<String, String> xpathMap = new HashMap<String, String>();

    static {
        xpathMap.put("hodnoceni", "//div[@id='itemhdr']//span[@class=\"word-rating\"]/text()");
        xpathMap.put("zavady a opravy", "//div[@class='fltL']/p[1]/text()");
        xpathMap.put("text", "//div[@class='fltL']//span[@itemprop=\"reviewBody\"]/text()");
        xpathMap.put("titulek", "//div[@class='listitem']/a/strong");
    }
 // tady je treba zmenit cesta resp. nazev adresare, kam se uklada vystup
    private static final String STORAGE = "./storage/autorecenze/";
 /* sem je nutno dat statickou cast webove adresy, ze ktere se budou cist odkazy
  * (dynamicka je nize kolem radku 85 v cyklu)  
  */
    private static final String SITE = "http://www.cars.cz/autorecenze/auta/vse";

    private static final int TEST_RUN_SIZE = 20;
    private static final int FULL_RUN_SIZE = 1000;
    private static final boolean TEST_RUN = true; //todo

    /**
     * Be polite and don't send requests too often.
     * Waiting period between requests. (in milisec)
     */
    private static final int POLITENESS_INTERVAL = 1000;
    private static final Logger log = Logger.getLogger(Crawler.class);

    /**
     * Main method
     */
    public static void main(String[] args) {
        //Initialization
        BasicConfigurator.configure(); //konfigurace logovani
        Logger.getRootLogger().setLevel(Level.INFO);
        File outputDir = new File(STORAGE);
        if (!outputDir.exists()) {
            boolean mkdirs = outputDir.mkdirs();
            if (mkdirs) {
                log.info("Output directory created: " + outputDir);
            } else {
                log.error("Output directory can't be created! Please either create it or change the STORAGE parameter.\nOutput directory: " + outputDir);
            }
        }
        HTMLDownloader downloader = new HTMLDownloader();
        Map<String, Set<String>> results = new LinkedHashMap<String, Set<String>>();
        ArrayList<Review> rev = new ArrayList<Review>();

        for (String key : xpathMap.keySet()) {
            Set<String> list = new LinkedHashSet<String>();
            results.put(key, list);
        }

        Set<String> urlsSet = new LinkedHashSet<String>();

        //Try to load links
        File links = new File(STORAGE + "_urls.txt");
        if (links.exists()) {
            try {
                urlsSet.addAll(Utils.readTXTFile(new FileInputStream(links)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        //If there isn't a saved file with links.
        //Extract links to pages with reviews.
        if (urlsSet.size() <= 0) {

            //Set pages max id
            int maxID;
            if (TEST_RUN) {
                maxID = TEST_RUN_SIZE;
            } else {
                maxID = FULL_RUN_SIZE;
            }
//            maxID = 3308; // actual pages max id
            for (int i = 0; i <= maxID; i+=20) {
            	//sem dat vyraz pro prochazeni jednotlivych stranek s vysledkama
                String link = SITE + "/?offset=" + i + "&order=comp_date_desc";
                //Use Xpath expression to extract links to review pages
                urlsSet.addAll(downloader.getLinks(link, "//div[@id='itemlist']/div/a/@href"));
                try {
                    Thread.sleep(POLITENESS_INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
// If links aren't saved do so, otherwise save a new file with time tag just to be sure
            if (!links.exists()) {
                Utils.saveFile(links, urlsSet);
            } else {
                Utils.saveFile(new File(STORAGE + Utils.SDF.format(System.currentTimeMillis()) + "_urls_size_" + urlsSet.size() + ".txt"),
                        urlsSet);
            }
        }
        // Save links that failed in some way.
        // Be sure to go through these and explain why the process failed on these links.
        // Try to eliminate all failed links - they consume your time while crawling data.
        reportProblems(downloader.getFailedLinks());
        downloader.emptyFailedLinks();

        for (String link : urlsSet) {
            if (!link.contains(SITE)) {
                link = SITE + link;
            }
            //Download and extract data according to xpathMap
            /* Tady jsem vymenil hashmap za list, abych to mohl pak postupně nandat 
             * do toho xml. Pri kazde iteraci se do seznamu prida jedna recenze
             * v podobe objektu typu Review.*/
            List<Review> products = downloader.processUrl(link, xpathMap);
            if (products.get(0) != null){
            rev.add(products.get(0));
            }
            
            try {
                Thread.sleep(POLITENESS_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // Save links that failed in some way.
        // Be sure to go through these and explain why the process failed on these links.
        // Try to eliminate all failed links - they consume your time while crawling data.
        reportProblems(downloader.getFailedLinks());
        downloader.emptyFailedLinks();
        log.info("-----------------------------");
        // Print some information.
        DomXmlOutput dEx = new DomXmlOutput();
        dEx.createRootElement("root");
        for (Review review : rev){
        	dEx.createRecenzeElement("recenze");
        	dEx.createOpravyElement("opravy", review.repairing);
        	dEx.createShrnutiElement("shrnuti", review.summary);
        	dEx.createBodyElement("text", review.body);
        }
        
        String xmlString = dEx.createXML();
        
        Utils.saveFile(new File(STORAGE + "/" + Utils.SDF.format(System.currentTimeMillis()) + 
        		"_xml_recenze_size_" + rev.size() + ".xml"), xmlString);
        
        log.info("Pocet recenzi: " + rev.size());
        
        System.exit(0);
    }

    /**
     * Save file with failed links for later examination.
     *
     * @param failedLinks links that couldn't be downloaded, extracted etc.
     */
    private static void reportProblems(Set<String> failedLinks) {
        if (!failedLinks.isEmpty()) {

            Utils.saveFile(new File(STORAGE + Utils.SDF.format(System.currentTimeMillis()) + "_undownloaded_links_size_" + failedLinks.size() + ".txt"),
                    failedLinks);
            log.info("Failed links: " + failedLinks.size());
        }
    }


}
