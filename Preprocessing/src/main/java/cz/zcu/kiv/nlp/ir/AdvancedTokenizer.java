/**
 * Copyright (c) 2014, Michal Konkol
 * All rights reserved.
 */
package cz.zcu.kiv.nlp.ir;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Michal Konkol
 */
public class AdvancedTokenizer implements Tokenizer {
    //cislo |  | html | tecky a sracky
    public static final String defaultRegex = "(\\d+[.,](\\d+)?)|" /* bere skupinu  obsahujici 1 cislici,
    za kterou nasleduje tecka nebo carka, za kterou nasleduje 1 skupina cislic nebo nic. Takze bere 
    i napr. 0. nebo 0, */ 
    		+ "([\\p{L}\\d]+)|" /* bere skupinu, kde je jakejkoli unicode znak z kategorie pismeno 
    		nebo cislice opakujici se jednou nebo vickrat */
    		+ "(<.*?>)|" /* bere skupinu, ktera zacina < a konci > (HTML) */
    		+ "([\\p{Punct}])"; // neco z tohohle: !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~

    public static String[] tokenize(String text, String regex) {
        Pattern pattern = Pattern.compile(regex); // vytvoreni paterny z reg. vyrazu pro zpracovani retezce

        ArrayList<String> words = new ArrayList<String>();

        Matcher matcher = pattern.matcher(text); // kontrola vstupniho retezce pomoci vytvorene paterny
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();

            words.add(text.substring(start, end)); // pridani oddeleneho tokenu (slova) do pole
        }

        String[] ws = new String[words.size()]; // vytvoreni pole o velikosti ArrayListu words
        ws = words.toArray(ws); // presun obsahu listu words do nove vytvoreneho pole

        return ws;
    }

    public static String removeAccents(String text) {
        return text == null ? null : Normalizer.normalize(text, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    @Override
    public String[] tokenize(String text) {
        return tokenize(text, defaultRegex);
    }
}
