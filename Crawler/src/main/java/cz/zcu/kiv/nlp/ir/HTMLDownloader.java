package cz.zcu.kiv.nlp.ir;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.fetcher.PageFetchResult;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.parser.ParseData;
import edu.uci.ics.crawler4j.parser.Parser;
import edu.uci.ics.crawler4j.url.WebURL;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import us.codecraft.xsoup.Xsoup;

import java.util.*;
import java.util.Map.Entry;

/**
 * This class is a demonstration of how crawler4j can be used to download a
 * website Created by Tigi on 31.10.2014.
 */
public class HTMLDownloader {

	private static final Logger log = Logger.getLogger(Crawler.class);
	private final Parser parser;
	private final PageFetcher pageFetcher;
	private Set<String> failedLinks = new HashSet<String>();

	/**
	 * Get failed links.
	 * 
	 * @return failed links
	 */
	public Set<String> getFailedLinks() {
		return failedLinks;
	}

	/**
	 * Empty the empty links set
	 */
	public void emptyFailedLinks() {
		failedLinks = new LinkedHashSet<String>();
	}

	/**
	 * Constructor
	 */
	public HTMLDownloader() {
		CrawlConfig config = new CrawlConfig();
		parser = new Parser(config);
		pageFetcher = new PageFetcher(config);

		config.setMaxDepthOfCrawling(0);
		config.setResumableCrawling(false);
	}

	/**
	 * Downloads given url
	 * 
	 * @param url
	 *            page url
	 * @return object representation of the html page on given url
	 */
	private Page download(String url) {
		WebURL curURL = new WebURL();
		curURL.setURL(url);
		PageFetchResult fetchResult = null;
		Page page;
		try {
			fetchResult = pageFetcher.fetchHeader(curURL);
			if (fetchResult.getStatusCode() == HttpStatus.SC_MOVED_PERMANENTLY) {
				curURL.setURL(fetchResult.getMovedToUrl());
				fetchResult = pageFetcher.fetchHeader(curURL);
			}
			if (fetchResult.getStatusCode() == HttpStatus.SC_OK) {
				try {
					page = new Page(curURL);
					fetchResult.fetchContent(page);
					if (parser.parse(page, curURL.getURL())) {
						return page;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} finally {
			if (fetchResult != null) {
				fetchResult.discardContentIfNotConsumed();
			}
		}
		return null;
	}

	/**
	 * Downloads given url page and extracts xpath expressions.
	 * 
	 * @param url
	 *            page url
	 * @param xpathMap
	 *            pairs of description and xpath expression
	 * @return pairs of descriptions and extracted values
	 */
	// Metoda predelana, aby vracela list misto hashmapy
	public List<Review> processUrl(String url, Map<String, String> xpathMap) {
		
		// Seznam objektu typu Review pro ukladaní casti recenzi
		ArrayList<Review> rev = new ArrayList<Review>();

		// Vytvoreni objektu typu Review pro ulozeni potrebnych informaci z recenze
		Review review = new Review();

		log.info("Processing: " + url);
		Page page = download(url);
		if (page != null) {
			ParseData parseData = page.getParseData();
			if (parseData != null) {
				if (parseData instanceof HtmlParseData) {
					Document document = Jsoup.parse(((HtmlParseData) parseData).getHtml());

					for (String key : xpathMap.keySet()) {
						ArrayList<String> list = new ArrayList<String>();
						list.addAll(Xsoup.compile(xpathMap.get(key)).evaluate(document).list());
						
						/* Zadani jednotlivych informaci do objektu Review 
						 * na zaklade informace sdruzene s klicem vstupni
						 * hashmapy metody processUrl
						 */
						if (key.equals("hodnoceni")) {
							review.summary = list.get(0);
						}
						if (key.equals("zavady a opravy")) {
							review.repairing = list.get(0);
						}
						if (key.equals("text")) {
							review.body = " " + list.get(0) + " ";
						}

					}

				}
			} else {
				log.info("Couldn't parse the content of the page.");
			}
		} else {
			log.info("Couldn't fetch the content of the page.");
			failedLinks.add(url);
		}
		// Pridani objektu do seznamu a jeho vraceni do tridy Crawler
		rev.add(review);
		return rev;
	}

	/**
	 * Downloads given url page and extracts xpath expression.
	 * 
	 * @param url
	 *            page url
	 * @param xPath
	 *            xpath expression
	 * @return list of extracted values
	 */
	public List<String> getLinks(String url, String xPath) {
		ArrayList<String> list = new ArrayList<String>();
		log.info("Processing: " + url);
		Page page = download(url);
		if (page != null) {
			ParseData parseData = page.getParseData();
			if (parseData != null) {
				if (parseData instanceof HtmlParseData) {
					Document document = Jsoup.parse(((HtmlParseData) parseData).getHtml());
					List<String> xlist = Xsoup.compile(xPath).evaluate(document).list();
					list.addAll(xlist);
				}
			} else {
				log.info("Couldn't parse the content of the page.");
			}
		} else {
			log.info("Couldn't fetch the content of the page.");
			failedLinks.add(url);
		}
		return list;
	}
}
